import React, { Suspense } from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import './i18next.js'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Suspense className="ciricli" fallback="Loading.....">
      <App />
    </Suspense>
  </React.StrictMode>
)
